#!/bin/sh

rsync ../yc-pyutils/docs/html/ yc-pyutils -r --delete --verbose
rsync ../yc-config/javadoc/ yc-config -r --delete --verbose
rsync ../ark-sage/javadoc/ ark-sage -r --delete --verbose
